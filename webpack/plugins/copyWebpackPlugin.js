// ---------------------------------------
// @Copy files from src to build
// ---------------------------------------
const CopyWebpackPlugin = require("copy-webpack-plugin");
const path = require("path");
const manifest = require("../manifest");
console.log(process.env);
module.exports = new CopyWebpackPlugin({
  patterns: [
    {
      from: path.resolve(manifest.paths.input, "minigame"),
      to: path.resolve(manifest.paths.output, "minigame"),
    },
    {
      from: path.resolve(manifest.paths.input, `assets/${process.env.THUMBNAIL}`),
      to: path.resolve(manifest.paths.output, `assets/${process.env.THUMBNAIL}`)
    }
  ],
});

