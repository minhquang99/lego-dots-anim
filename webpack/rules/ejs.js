const path = require("path");
const { paths } = require("../manifest");
module.exports = {
   test: /\.(ejs)$/,
   use: [
      {
         loader: "ejs-compiled-loader",
         options: {
            htmlmin: true,
            htmlminOptions: {
              removeComments: true
            }
         }
      },
   ],
};
