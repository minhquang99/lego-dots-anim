module.exports = [
    require('./js'),
    require('./sass'),
    require('./fonts'),
    require('./images'),
    require('./videos'),
    require('./ejs'),
    require('./doc')
];