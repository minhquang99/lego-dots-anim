const path = require("path");

// -----------------
// @DevServer Config
// -----------------

const devServer = {
  open: false,
  static: {
    directory: path.resolve(__dirname, "../src"),

  },
  watchFiles: ["../src/**/*.ejs", "../src/*.ejs", "../src/**/*.js"],
  liveReload: true,
  port: 8080,
};

// -----------------
// @Exporting Module
// -----------------

module.exports = devServer;
