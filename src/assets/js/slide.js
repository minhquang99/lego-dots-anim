$(".owl-carousel-slide").owlCarousel({
    loop: true,
    autoplay: true,
    slideTransition: 'linear',
    autoplayTimeout: 1500,
    autoplaySpeed: 1500,
    // autoplayHoverPause: true,
    margin: 0,
    nav: false,
    dots: false,
    responsive: {
        0: {
            items: 2
        },
        414: {
            items: 2.5
        },
        600: {
            items: 3.5
        },
        750: {
            items: 4
        },
        1200: {
            items: 6
        }
    }
})