import { windowOnLoad } from "./utils";
import { customGSAPAnims } from './custom-gsap';

let anim_open = new customGSAPAnims("openController", "openTimeline", [
    {
        target: "girl-before",
        time: 0.5,
        xAxis: "-100%",
        yAxis: "0%",
        position: 1
    },
    {
        target: "pants-before",
        time: 0.5,
        xAxis: "100%",
        yAxis: "0%",
        position: 1
    },
], "openScene", "100%", 0.45, ".section-split", -900);

let anim_close = new customGSAPAnims("closeController", "closeTimeline", [
    {
        target: "girl-before1",
        time: 0.5,
        xAxis: "100%",
        yAxis: "0%",
        position: 1
    },
    {
        target: "pants-before1",
        time: 0.5,
        xAxis: "-100%",
        yAxis: "0%",
        position: 1
    },
], "closeScene", "20%", 1, ".rabbit", 500);

windowOnLoad(() => {
    anim_open.gsapAnim();
    anim_close.gsapAnim();
})