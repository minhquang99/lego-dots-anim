import * as ScrollMagic from "scrollmagic";
import { TweenMax, TimelineMax } from "gsap";
import { ScrollMagicPluginGsap } from "scrollmagic-plugin-gsap";
import { Power2 } from "gsap";
import gsap from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";

ScrollMagicPluginGsap(ScrollMagic, TweenMax, TimelineMax);

gsap.registerPlugin(ScrollTrigger);

export class customGSAPAnims {
    constructor(controller, timeline, subjects, scene, duration, triggerHook, triggerElement, offSet) {
        this.controller = controller;
        this.timeline = timeline;
        this.subjects = subjects;
        this.scene = scene;
        this.duration = duration;
        this.triggerHook = triggerHook;
        this.triggerElement = triggerElement;
        this.offSet = offSet;
    }

    gsapAnim() {
        this.controller = new ScrollMagic.Controller();
        this.timeline = new TimelineMax();
        var time = this.timeline;

        this.subjects.forEach(function (item) {
            time
            .to(
                `.${item.target}`, item.time, 
                {
                    x: item.xAxis, 
                    y: item.yAxis,
                    ease: Power2.easeNone,
                }, item.position
            );
        });

        this.scene = new ScrollMagic.Scene({
            triggerElement: this.triggerElement,
            offset: this.offSet,
            duration: this.duration,
            triggerHook: this.triggerHook
        })
            .setTween(this.timeline)
            .addTo(this.controller);
    
            this.scene.on("start", function () {
                console.log("Action");
            });
    }
}